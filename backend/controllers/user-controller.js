const db = require("../models")
const bcrypt = require("bcrypt")
const jwt = require("jsonwebtoken")
const fs = require('fs')
const passwordValidator = require('password-validator');

//Vérification du mot de passe dans le format choisi. 
//Ici, 1 majuscule, 1 chiffre, minimum 8 caractères 
//et max. 100 caractères
const schema = new passwordValidator();
schema.is().min(8)
    .is().max(100)
    .has().uppercase(1)
    .has().lowercase()
    .has().digits(1)
    .has().not().spaces()

// async await try catch 
exports.signup = async (req, res) => {
    try {
        const user = {
            email: req.body.email,
            password: req.body.password,
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            isAdmin: req.body.isAdmin
        }
        // Validation de requête
        if (!user.email) {
            return res.status(400).send({
                message: "le champ email est obligatoire!"
            });
        };
        const userDb = await db.User.findOne({
            where: {
                email: user.email
            }
        })
        if (userDb) {
            return res.status(401).json({
                message: "Utilisateur déjà enregistré"
            })
        } else {
            if (!schema.validate(user.password)) {
                return res.status(400).json({
                    message: "Mot de passe requis: 8 caractères minimum, 1 majuscule, 1 minuscule et sans espace"
                })
            }
            bcrypt.hash(user.password, 10)
                .then(hash => {
                    user.password = hash
                    db.User.create(user)
                    return res.status(200).json({
                        message: "Utilisateur créé"
                    })
                })
                .catch(() => {
                    res.status(500).json({
                        message: "Impossible de se connecter"
                    })
                })
        }

    } catch (error) {
        res.status(500).json({
            message: error.message
        })
    }
}

//affichage de tous les utilisateurs
exports.getAll = res => {
    db.User.findAll({
            include: [{
                    model: db.Post
                },
                {
                    model: db.Comment
                }
            ],
            attributes: {
                exclude: ["password"]
            }
        })
        .then(allUsers => {
            res.status(200).send(allUsers)
        })
        .catch(() => {
            res.status(500).json({
                message: "Impossible d'afficher les utilisateurs"
            })
        })
}

//connexion de l'utilisateur et génération du Token
exports.login = (req, res) => {
    //console.log(req.body);
    db.User.findOne({
            where: {
                email: req.body.email
            }
        })
        .then(user => {
            if (!user) {
                return res.status(404).json({
                    message: 'Utilisateur non trouvé !'
                });
            }
            bcrypt.compare(req.body.password, user.password)
                .then(valid => {

                    if (!valid) {
                        return res.status(400).send({
                            error: 'Mot de passe incorrect !'
                        });
                    }
                    res.status(200).json({
                        token: jwt.sign({
                                userId: user.id,
                                isAdmin: user.isAdmin
                            },
                            'RANDOM_TOKEN_SECRET', {
                                expiresIn: '24h'
                            }
                        )
                    });
                })
                .catch(() => res.status(500).json({
                    message: "Impossible de se connecter"
                }));
        })
        .catch(() => res.status(500).json({
            message: "Impossible de se connecter"
        }))
}

//modification d'un utilisateur
exports.modify = (req, res) => {
    const id = req.params.id
// Recherche d'un utilisateur en fonction de son id
    db.User.findOne({
            where: {
                id: id
            },
//on exclu le mot de passe de l'utilisateur pour plus de sécurité
            attributes: {
                exclude: ["password"]
            }
        })
        .then(userToUpdate => {
// verification authorisation
            if (req.user.userId !== userToUpdate.id) {
                res.status(403).send({
                    message: "Modification non autorisée!"
                })
            }
// verification modification image de profil
            if (req.file) {
                const filename = userToUpdate.image.split('/images/')[1]
                if (userToUpdate.image.indexOf('default') == -1) {
                    fs.unlink(`images/${filename}`, () => {})
                }
                userToUpdate.image = `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
            }
// Modification de l'utilisateur
            db.User.update({
                    firstname: req.body.firstname,
                    lastname: req.body.lastname,
                    image: userToUpdate.image
                }, {
                    where: {
                        id: id
                    }
                })
                .then(() => {
                    res.status(200).json({
                        message: "Utilisateur mis à jour"
                    })
                })
                .catch(() => {
                    res.status(500).json({
                        message: "Impossible de modifier l'utilisateur"
                    })
                })
        })
}

//suppression de l'utilisateur
exports.delete = ((req, res) => {
    const id = req.params.id;
    db.User.findOne({
            where: {
                id: id
            },
            attributes: {
                exclude: ["password"]
            }
        })
        .then(userToDelete => {
            if (req.user.userId !== userToDelete.id) {
                res.status(403).send({
                    message: "Modification non autorisée!"
                })
            }
            const filename = userToDelete.image.split('/images/')[1]
            if (userToDelete.image.indexOf('default') == -1) {
                fs.unlink(`images/${filename}`, () => {})
            }
            db.User.destroy({
                    where: {
                        id: id
                    }
                })
                .then(() => {
                    res.status(200).json({
                        message: "Utilisateur supprimé"
                    })
                })
                .catch(() => {
                    res.status(500).send({
                        message: "Impossible de supprimer l'utilisateur"
                    });
                });


        })

})
// afficher un utilisateur
exports.getOne = (req, res) => {
    const id = req.params.id
    db.User.findOne({
            where: {
                id: id
            },
            attributes: {
                exclude: ["password"]
            }
        })
        .then(userToShow => {
            if (userToShow.id == req.user.userId || req.user.isAdmin) {
                res.status(200).send(userToShow)
            }
        })
        .catch(() => res.status(500).send({
            message: "Impossible d'afficher l'utilisateur"
        }))
}