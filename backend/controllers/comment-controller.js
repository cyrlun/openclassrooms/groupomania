const db = require("../models");


//création d'un commentaire
exports.create = (req, res) => {
    db.Comment.create({
            message: req.body.message,
            UserId: req.body.UserId,
            PostId: req.body.PostId
        })
        .then(newPost => res.status(200).send(newPost))
        .catch(() => res.status(500).send({
            message: "Impossible de créer un commentaire"
        }))
}

//suppression d'un commentaire
exports.delete = (req, res) => {
    const id = req.params.id;
    db.Comment.findOne({
            where: {
                id: id
            }
        })
        .then(comment => {
            if (req.user.userId == comment.UserId || req.user.isAdmin) {
                comment.destroy()
                    .then(() => {
                        res.status(200).json({
                            message: "Commentaire supprimé"
                        })
                    })
                    .catch(err => {
                        res.status(500).send({
                            message: "Impossible de supprimer le commentaire"
                        });
                    });
            } else {
                res.status(403).send({
                    message: "Suppression du commentaire non autorisée"
                });
            }
        })

}

//modification d'un commentaire
exports.modify = (req, res) => {
    const id = req.params.id
    db.Comment.findOne({
            where: {
                id: id
            }
        })
        .then(comment => {
            if (req.user.userId == comment.UserId) {
                comment.update({
                        message: req.body.message
                    })
                    .then(() => res.status(200).json({
                        message: "Commentaire modifié"
                    }))
                    .catch(err => {
                        res.status(500).json({
                            message: "Modification impossible"
                        })
                    })
            } else {
                res.status(403).send({
                    message: "Modification du commentaire non autorisée"
                })
            }

        })
        .catch(() => res.status(500).send({
            message: "Commentaire introuvable"
        }))
}

exports.getAll = (req, res) => {
    const id = req.params.id
    db.Comment.findAll({
            where: {
                id: id
            }
        }, {
            include: [{
                model: db.User,
                attributes: {
                    exclude: ["password"]
                }
            }]
        })
        .then(allComments => res.status(200).send(allComments))
        .catch(() => res.status(500).send({
            message: "Impossible de récupérer les commentaires"
        }))
}