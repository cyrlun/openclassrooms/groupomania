const express = require('express')
const app = express()
const db = require('./models')



app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});


app.use(express.urlencoded({
    extended: true
}))
app.use(express.json())

app.use(express.static('images')); 
app.use('/images', express.static('images'));

const userRoutes = require("./routes/user-routes")
app.use('/api/users', userRoutes)

const postRoutes = require('./routes/post-routes')
app.use('/api/posts', postRoutes)

const commentRoutes = require("./routes/comment-routes")
app.use('/api/comments', commentRoutes)

db.sequelize.sync()

module.exports = app