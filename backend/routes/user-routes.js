const express = require("express")
const router = express.Router()
const userCtrl = require("../controllers/user-controller")

const auth = require('../middleware/auth');
const multer = require('../middleware/multer-config');

//création d'un nouvel utilisateur
router.post('/signup', userCtrl.signup)

//connexion de l'utilisateur et génération du token
router.post('/login', userCtrl.login)

//afficher un utilisateur
router.get('/:id', auth, userCtrl.getOne)

//Modifier un utilisateur
router.put('/update/:id', auth,multer, userCtrl.modify)

//supprimer utilisateur
router.delete('/delete/:id', auth, userCtrl.delete)


module.exports = router
