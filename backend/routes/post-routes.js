const express = require("express")
const router = express.Router()
const postCtrl = require('../controllers/post-controller')

const auth = require('../middleware/auth');
const multer = require('../middleware/multer-config');

// Création d'un nouveau post
router.post('/new',auth, multer, postCtrl.create)

//affichage d'un post
router.get('/:id', postCtrl.getOne)

//afficher tous les posts
router.get('/',auth, postCtrl.getAll)

//modification d'un post
router.put('/update/:id',auth, multer, postCtrl.modify)

//suppression d'un post
router.delete('/delete/:id',auth, postCtrl.delete)

module.exports = router
