const express = require("express")
const router = express.Router()
const commentCtrl = require('../controllers/comment-controller')
const auth = require('../middleware/auth')
const multer = require('../middleware/multer-config')

//création d'un nouveau commentaire
router.post('/new',auth, multer,commentCtrl.create)

//suppression d'un commentaire
router.delete('/delete/:id',auth, commentCtrl.delete)

//modification d'un commentaire
router.put('/update/:id',auth, commentCtrl.modify)

router.get('/:id',commentCtrl.getAll)

module.exports = router


