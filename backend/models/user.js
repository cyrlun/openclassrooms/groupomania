module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define("User",{
        email:{
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        password : {
            type: DataTypes.STRING,
            allowNull: false
            
        },
        firstname:{
            type: DataTypes.STRING,
            allowNull: false
        },
        lastname:{
            type: DataTypes.STRING,
            allowNull: false
        },
        image:{
            type: DataTypes.STRING,
            allowNull: true,
            defaultValue: 'http://localhost:3000/images/default.png'
        },
        isAdmin:{
            type:DataTypes.BOOLEAN,
            defaultValue: false,
            allowNull: false,
            set: function(value) {
                if (value == 'true') value = true;
                if (value == 'false') value = false;
                this.setDataValue('isAdmin',value)
            }
        }
    })

    User.associate = models => {
        User.hasMany(models.Post, {
            foreignKey:{
                allowNull: false
            },
            onDelete: "CASCADE"
        }),
        User.hasMany(models.Comment, {
            foreignKey:{
                allowNull: false
            },
            onDelete: "CASCADE"
        })
    }
    return User
}
