module.exports = (sequelize, DataTypes) => {
    const Comment = sequelize.define("Comment", {
        message: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        UserId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'Users',
                key: 'id'
            }
        },
        PostId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'Posts',
                key: 'id'
            }
        }
    })

    Comment.associate = models => {
        Comment.belongsTo(models.Post, {
            foreignKey: {
                allowNull: false,
                as: 'post'
            },
            onDelete: "CASCADE"
        });

        Comment.belongsTo(models.User, {
            foreignKey: {
                allowNull: false,
                as: 'user'
            },
            onDelete: "CASCADE"
        });
    }

    return Comment
}