module.exports = (sequelize, DataTypes) => {
    const Post = sequelize.define("Post",{
        title: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        content: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        image: {
            type: DataTypes.STRING,
            allowNull: true,
            defaultValue: ''
        },
        UserId:{
            type:DataTypes.INTEGER,
            allowNull: false,
            references:{
                model:'Users',
                key:'id'
            }
        }
    })

    Post.associate = models => {
        Post.belongsTo(models.User,{
            foreignKey: {
                allowNull: false
            },
            onDelete: "CASCADE"
        })
        Post.hasMany(models.Comment,{
            onDelete: "CASCADE"
        })
    }

    return Post
}
