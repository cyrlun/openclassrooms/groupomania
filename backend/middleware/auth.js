const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.verify(token, 'RANDOM_TOKEN_SECRET');
    const userId = decodedToken.userId;
    if (req.body.userId && req.body.userId !== userId) {
      throw 'Invalid user ID';
    } else {
      //permet par la suite de vérifier si l'userId enregistré 
      //dans le token est le même que celui de l'auteur de la requête
      req.user = decodedToken 
      next();
    }
  } catch (error) {
    res.status(401).json({
      message: error.message
    });
  }
};

