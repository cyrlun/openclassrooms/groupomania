# GROUPOMANIA - Projet 7 - Développeur Web OpenClassrooms
Le projet consiste à créer un réseau social (d'entreprise) pour Groupomania
***
![](https://img.shields.io/badge/-Javascript-yellow)
![](https://img.shields.io/badge/-NodeJs-green)
![](https://img.shields.io/badge/-VueJs-blueviolet)

Pour tester le projet, téléchargez le dossier.

Puis suivez les instructions ci-dessous pour lancer le projet

## FRONT-END

Ouvrez le dossier racine via un terminal

Tapez 
``` 
$ cd frontend 
$ npm install
$ npm run serve
```
Le port utilisé est localhost:8080, s'il n'est pas libre, il change automatiquement

## BACK-END

Ouvrez le dossier racine via un terminal

Tapez 
``` 
$ cd backend 
$ npm install
$ npm run serve
```
Le port par defaut est localhost:3000

## TESTER LE PROJET

Une fois ces manipulations faites, rendez-vous sur le lien suivant:

http://localhost:8080 

(Si le port 8080 était libre lors du lancement du serveur front, sinon, reportez-vous au lien affiché dans la console)
***
La création d'utilisateur ADMIN n'est possible que via un outil externe!

