import Vue from 'vue'
import VueRouter from 'vue-router'
import Inscription from '../components/Inscription/Inscription.vue'
import Connexion from '../components/Connexion/Connexion.vue'
import Home from '../components/Home/Home.vue'
import EditProfile from '../components/EditProfile/EditProfile.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/inscription',
    name: 'Inscription',
    component: Inscription
  },
  {
    path: '/connexion',
    name: 'Connexion',
    component: Connexion
  },
  {
    path:'/',
    name:'home',
    component: Home
  },
  {
    path:'/edit-profile',
    name:'EditProfile',
    component:EditProfile
  }
]

const router = new VueRouter({
  base:'/',
  mode:'history',
  routes
})

export default router
